# IRM
# Static modifiers


# FACTION

# Sector political consensus
faction_encouragement = {
	faction_happiness = 0.10
}


# FLEET / SHIP

# Assets optimization
mining_station_optimised = {
	shipsize_mining_station_upkeep_mult = -0.25
}
research_station_optimised = {
	shipsize_research_station_upkeep_mult = -0.25
}

#Military Training
ship_trained = {
	ship_starting_experience_add = 50
	ship_experience_gain_mult = 0.25
}

# Fleet Tactical Protocols
fleet_trained = {
	ship_fire_rate_mult = 0.10
	ship_evasion_mult = 0.10
}


# PLANET

# Former Capital
former_capital = {
	pop_happiness = -0.10

	icon = "gfx/interface/icons/planet_modifiers/pm_planet_from_space.dds"
	icon_frame = 3
}

# Sector Capital
sector_capital = {
	pop_government_ethic_attraction = 0.50

	icon = "gfx/interface/icons/planet_modifiers/pm_planet_from_space.dds"
	icon_frame = 1
}


# POP

# Suppressed Pop
pop_suppressed = {
	pop_happiness = -0.20
	pop_government_ethic_attraction = 1.00
}