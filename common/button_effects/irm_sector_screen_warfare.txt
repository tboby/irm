# IRM
# Buttons for Sector warfare settings


# POLICIES


# Policy - Disallow Fleet
ui_cb_pol_nofleet_on = {
	potential = {
		exists = event_target:uiSectorCapital
		event_target:uiSectorCapital.sector = { sector_pol_nofleet = no }
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.pol_nofleet.length"
			event_target:uiSectorDatabase = {
				check_variable = { which = nPolicyNofleet value = 0 }
			}
		}
	}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.pol_nofleet.desc"
		hidden_effect = {
			event_target:uiSectorDatabase = {
				set_variable = { which = bPolicyNofleet value = 1 }
				set_variable = { which = nPolicyNofleet value = @monthsPolicyLength }
			}
			# We'll try to disband sector fleet, if exists
			if = {
				limit = { exists = event_target:uiSectorMilFleet }
					event_target:uiSectorMilFleet = {
						fleet_do_disband = yes
					}
			}
		}
	}
}
ui_cb_pol_nofleet_off = {
	potential = {
		exists = event_target:uiSectorCapital
		event_target:uiSectorCapital.sector = { sector_pol_nofleet = yes }		
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.pol_nofleet.length"
			event_target:uiSectorDatabase = {
				check_variable = { which = nPolicyNofleet value = 0 }
			}
		}
	}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.pol_nofleet.desc"
		hidden_effect = {
			event_target:uiSectorDatabase = {
				set_variable = { which = bPolicyNofleet value = 0 }
			}
		}
	}	
}

# Policy - Disallow Army
ui_cb_pol_noarmy_on = {
	potential = {
		exists = event_target:uiSectorCapital
		event_target:uiSectorCapital.sector = { sector_pol_noarmy = no }
	}
	allow = { always = no }
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.pol_noarmy.desc"
		hidden_effect = {
			event_target:uiSectorDatabase = {
				set_variable = { which = nPolicyNoarmy value = 1 }
			}
		}
	}
}
ui_cb_pol_noarmy_off = {
	potential = {
		exists = event_target:uiSectorCapital
		event_target:uiSectorCapital.sector = { sector_pol_noarmy = yes }		
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.pol_noarmy.length"
			event_target:uiSectorCapital.sector = { sector_pol_noarmy = no }
		}
	}
}



# SETTINS

ui_cb_set_aggressive_na = {
	potential = { not = { exists = event_target:uiSectorMilFleet } }
	allow = { custom_tooltip = { fail_text = "sector_screen.ui.block.war.set.none" always = no } }	
}
# Fleet - Set Aggressice
ui_cb_set_aggressive_on = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { not = { check_variable = { which = nFleetStance value = @nStanceAggressive } } }
	}
	allow = {}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.set_aggressive.desc"
		hidden_effect = {
			event_target:uiSectorMilFleet = {
				set_fleet_stance = aggressive
				set_aggro_range_measure_from = self
				set_aggro_range = @nStanceAggroRange			
				set_variable = { which = nFleetStance value = @nStanceAggressive }
			}
		}
	}
}
ui_cb_set_aggressive_off = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { check_variable = { which = nFleetStance value = @nStanceAggressive } }
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.set_stance.fail"
			always = no
		}		
	}
}

# Fleet - Set Passive
ui_cb_set_passive_na = {
	potential = { not = { exists = event_target:uiSectorMilFleet } }
	allow = { custom_tooltip = { fail_text = "sector_screen.ui.block.war.set.none" always = no } }	
}
ui_cb_set_passive_on = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { not = { check_variable = { which = nFleetStance value = @nStancePassive } } }
	}
	allow = {}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.set_passive.desc"
		hidden_effect = {
			event_target:uiSectorMilFleet = {
				set_fleet_stance = passive
				set_variable = { which = nFleetStance value = @nStancePassive }
			}
		}
	}
}
ui_cb_set_passive_off = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { check_variable = { which = nFleetStance value = @nStancePassive } }
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.set_stance.fail"
			always = no
		}		
	}
}

# Fleet - Bombardment
ui_cb_set_bomb_na = {
	potential = { not = { exists = event_target:uiSectorMilFleet } }
	allow = { custom_tooltip = { fail_text = "sector_screen.ui.block.war.set.none" always = no } }	
}
ui_cb_set_bomb_on = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { not = { check_variable = { which = bBombardment value = 1 } } }
	}
	allow = {}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.set_bomb.desc"
		hidden_effect = {
			event_target:uiSectorMilFleet = {
				set_fleet_bombardment_stance = selective
				set_variable = { which = bBombardment value = 1 }
			}
		}
	}
}
ui_cb_set_bomb_off = {
	potential = {
		exists = event_target:uiSectorMilFleet
		event_target:uiSectorMilFleet = { check_variable = { which = bBombardment value = 1 } }
	}
	allow = {}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.set_bomb.desc"
		hidden_effect = {
			event_target:uiSectorMilFleet = {
				set_fleet_bombardment_stance = disabled
				set_variable = { which = bBombardment value = 0 }
			}
		}
	}	
}

# Fleet - Home Location
ui_cb_set_home_na = {
	potential = { not = { exists = event_target:uiSectorMilFleet } }
	allow = { custom_tooltip = { fail_text = "sector_screen.ui.block.war.set.none" always = no } }	
}
ui_cb_set_home_on = {
	potential = {
		exists = event_target:uiSectorMilFleet
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.set_home.req.object"
			event_target:uiSectorMilFleet = {
				or = {
					exists = solar_system
					exists = orbit
				}
			}
		}
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.set_home.req.unset"
			event_target:uiSectorMilFleet = {
				nor = {
					and = {
						exists = solar_system
						solar_system.star = { event_target:uiSectorMilFleet = { has_fleet_flag = home_id_@prev }  }
					}
					and = {
						exists = orbit
						orbit = { event_target:uiSectorMilFleet = { has_fleet_flag = home_id_@prev }  }
					}					
				}
			}
		}				
	}
	effect = {
		custom_tooltip = "sector_screen.ui.block.war.set_home.desc"
		hidden_effect = {
			event_target:uiSectorMilFleet = {
				if = {
					limit = {
						exists = solar_system
						exists = orbit
					}
					orbit = {
						event_target:uiSectorMilFleet = {
							# Clear all possible previous home locations
							every_planet  ={
								limit = { prev = { has_fleet_flag = home_id_@prev } }
									prev = { remove_fleet_flag = home_id_@prev }
							}
							# Assign new home location
							set_fleet_flag = home_id_@prev
						}
					}
					else = {
						if = {
							limit = { exists = solar_system }
								solar_system.star = {
									event_target:uiSectorMilFleet = {
										# Clear all possible previous home locations
										every_planet  ={
											limit = { prev = { has_fleet_flag = home_id_@prev } }
												prev = { remove_fleet_flag = home_id_@prev }
										}
										# Assign new home location
										set_fleet_flag = home_id_@prev
									}
								}
						}
					}
				}
			}
		}
	}
}



# TOOLTIPS

# Sector Naval Limit
ui_tt_sector_naval_lim = {
	potential = {
		exists = event_target:uiSectorDatabase
	}
	allow = {
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.inf_naval.desc"
			always = no
		}
		custom_tooltip = {
			fail_text = "sector_screen.ui.block.war.inf_naval.tooltip.base"
			always = no
		}
		custom_tooltip = {
			success_text = "sector_screen.ui.block.war.inf_naval.tooltip.colony"
			event_target:uiSectorDatabase = { check_variable = { which = nSectorNavalLimColony value > 0 } }
		}
		custom_tooltip = {
			success_text = "sector_screen.ui.block.war.inf_naval.tooltip.pop"
			event_target:uiSectorDatabase = { check_variable = { which = nSectorNavalLimPop value > 0 } }
		}								
	}
}